==
CV
==

-------------------------
Personal Curriculum Vitae
-------------------------

This repository is dedicated to my Curriculum Vitae.
Unless you have the particular need to print it, please consult the `web version <https://cv.shukuru.world/>`_ and
save a bunch of trees ;)

You can also download a PDF version here: `Download <https://gitlab.com/shukuru/cv/-/jobs/artifacts/main/raw/cv.pdf?job=pdf>`_

Thanks!

--------
Material
--------

This static page is generated with `Sphinx <https://www.sphinx-doc.org/en/master/>`_.

Contact icons are from `Icones8 collection <https://icones8.fr/icons>`_.