.. title:: Programmeur backend, système et réseaux

.. image:: /_static/icons8-gitlab-48.png
    :target: https://gitlab.com/shukuru
.. image:: /_static/icons8-twitter-48.png
    :target: https://twitter.com/ShukuruWorld
.. image:: /_static/icons8-mail-48.png
    :target: mailto:fd@shukuru.world

.. note:: 
    
    Une version raccourcie au format PDF est disponible `ici <https://gitlab.com/shukuru/cv/-/jobs/artifacts/main/raw/cv.pdf?job=pdf>`_

----

.. figure:: /_static/me.jpg
    :align: left
    :alt: Picture of me thinking
    :scale: 20

Programmeur autodidacte, passionné depuis de longues années, je suis en quête perpétuelle de savoir,
aussi bien dans le domaine des technologies numériques qu'en sciences physiques.

Connaître le fonctionnement des boîtes noires qui nous entourent au quotidien est pour
moi une nécessité ainsi qu'une véritable source de défis.
Ce dont pourquoi j'affectionne particulièrement le domaine de **l'électronique**, des **systèmes
d'exploitation** ainsi que des **communications** et aspire à devenir *kernel hacker* sur mon temps libre.

Résoudre des problèmes à un plus haut niveau d'abstraction, notamment pour le web orienté backend, 
est tout aussi intéressant lorsque cela permet **d'automatiser des tâches répétitives** 
ou tout simplement de **rendre la vie des gens plus agréable et fun** :)

🔴 Vie personnelle
^^^^^^^^^^^^^^^^^^
32 ans, actuellement **pacsé et père de deux enfants**, j'ai obtenu mon Brevet des Collèges
en 2004 puis cessé ma scolarité en 2007 pour m'engager aux sein des forces armées Françaises
en 2009 jusqu'en 2022.

Je n'ai jamais vraiment réussi à m'adapter au système scolaire et **préfère apprendre les 
choses par moi-même** ou par expérimentation du sujet.

Mon expérience au sein des Armées a été fort en **qualité d'ouverture d'esprit**, de **rigueur** et 
**d'adaptabilité**. J'y ai effectué plusieurs missions notamment au Tchad, au Liban ainsi qu'à
Djibouti.

🔴 Expériences professionnelles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**2009-2017**
    🧭 *Topographe pour application militaire* - **Ministère des Armées**

    .. admonition:: Description du poste

        En avance de phase, l'équipe de reconnaissance établie un périmètre
        de sécurité sur la position de tir et effectue également
        une reconnaissance d'axes routiers afin de prévenir toute attaque
        ou intrusion.

        Le topographe quand à lui, est chargé d'orienter correctement les pièces
        d'artillerie, sur le plan X et Y, dans l'axe de tir en se référant au nord magnétique
        et en intégrant dans ses calculs diverses variables optiques et météorologiques.
    
    Expérience très intéressante, m'a permit **d'acquérir une certaine rigueur** ainsi
    qu'une **grande faculté d'adaptabilité**.

**2017-2019**
    📦 *Magasinier matériel personnel* - **Ministère des Armées**
    
    .. admonition:: Description du poste

        Assure la distribution du matériel et effectue un contrôle des stocks
        disponibles.
    
    Changement d’environnement radical pour des raisons de convenance personnelle,
    ce poste m'a également **demandé de la rigueur**. J'y ai également mis mes
    **compétences en programmation à contribution** afin de faciliter le suivi des colis 
    pour l'ensemble de l'équipe.

**2019-2022**
    ⌨️ *Développeur backend, système et réseaux* - **Ministère des Armées**

    .. admonition:: Description du poste

        Analyse le besoin et le transpose en cahier des charges chiffré en temps, 
        implémente et maintien la solution informatique transposée en effectuant quotidiennement 
        une MCO-MCS et tiens également la documentation à jour.
    
    Ce poste résume la nécessité pour moi de **vouloir exercer ma passion** de toujours dans
    un cadre beaucoup **plus formel et professionnel**.
    
    **Au-delà des attentes classiques** d'un développeur, j'ai **conçu** quelques solutions en 
    **architecture micro-services** composées d'API web sur la stack **Python-Flask-SQLAlchemy**, 
    un **broker d'évènements Nagios en C** permettant de relayer les données de supervision 
    depuis un système d'informations isolé.
    
    Quelques besoins sur la couche frontend en **Javascript pur** (Pas de framework) et **Bootstrap**
    pour le CSS.

    J'ai également **composé avec les conteneurs** (Docker), **l'intégration et le déploiement
    continu** en concevant des **pipelines de CI/CD** avec leur DAG respectif.

    Pour chaque projet, j'ai eu le plaisir d'y **apporter une documentation pertinente** et
    d'en **effectuer le support ainsi que ses évolutions**.

🔴 Compétences techniques
^^^^^^^^^^^^^^^^^^^^^^^^^^
📜 **Langages de programmation**
    - C
    - Python
    - Assemblage x86

🌐 **Web**
    - Flask
    - SQLAlchemy
    
➿ **CI/CD**
    - GitLab CI (Pipelines, éxécuteurs auto-hébergés)
    - Harbor Registry/Notary/Trivy

🔌 **Réseau**
    - Protocole HTTP(S)
    - Protocole AMQP
    - gRPC
    - Sockets bas-niveau (TCP, UDP)
    - Nginx, WSGI

⚙️ **Système**
    - Linux (Fedora, Debian)
    - IPC (sockets UNIX)
    - Docker
    - Notions K8S
    - Nagios
    - Théorie des systèmes d'exploitation: *POST, séquence de boot, processus, exécutables, gestion de la mémoire*

📔 **Méthodologies**
    - TDD
    - Agile

🔴 Compétences en cours d'assimilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

⚙️ **Système**
    - Kubernetes

🔴 Projets personnels
^^^^^^^^^^^^^^^^^^^^^^
Vous pouvez consulter `mon espace GitLab <https://gitlab.com/shukuru>`_ ou 
`mes notes personnelles <https://unsignednotes.shukuru.world>`_.

Voici une liste de quelques projets, terminés ou non:
    - `Découpeur de fichiers (Python) <https://gitlab.com/shukuru/slice>`_
    - `Générateur de flux RSS (Python) <https://gitlab.com/shukuru/naranja-git>`_
    - `Unsigned Notes (Sphinx) <https://gitlab.com/shukuru/unsigned-notes>`_
    - `Bootloader stage 1 pour architecture x86 (Assemblage x86) <https://gitlab.com/shukuru/seed-bootloader>`_
    - `Désassembleur pour Intel 8080 <https://gitlab.com/shukuru/e8080>`_

Actuellement, je m'auto-forme sur Kubernetes, je base mon infrastructure auto-hébergée sur cette technologie
et j'ai déployé manuellement un cluster 1/3 sur un Fedora Server (KVM) exécutant des VM Fedora CoreOS. 

Je passe beaucoup plus de temps à expérimenter qu'à produire des projets
personnels, mais je fais au mieux pour accommoder cela à ma vie de famille et
professionnelle :)

🔴 Langues
^^^^^^^^^^^
    🇺🇸 Anglais: Lu, parlé (B2)

    🇪🇸 Espagnol: Lu, parlé (B1)

🔴 Centres d’intérêts
^^^^^^^^^^^^^^^^^^^^^^
J'ai une grande appétence pour les sciences physiques et chimiques, convaincu depuis
l'enfance que cela est essentiel.

L'électronique, particulièrement les machines "vintage" occupent également une partie
de mon temps libre.

Voyager permet l'ouverture d'esprit en rencontrant de nouvelles cultures et est également
important pour moi.

La rédaction également, elle me permet de refléter mes pensées et de comprendre lorsque
j'apprends de nouvelles choses, d'où cette idée de notes personnelles (cf. `Projets personnels <#projets-personnels>`_)

Et pour finir, musique et cinéma comme grand nombre de personnes ;)
