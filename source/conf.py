# -- Project information -----------------------------------------------------
project = 'CV'
copyright = '2022, Florent DUPONT'
author = 'Florent DUPONT'

release = 'v1.0.0'

# -- General configuration ---------------------------------------------------
extensions = []
templates_path = ['_templates']
language = 'fr'
exclude_patterns = []
html_title = 'Florent DUPONT'
html_theme = 'haiku'
html_static_path = ['_static']
html_output = 'public'
