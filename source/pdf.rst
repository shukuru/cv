..
    PDF version

Florent DUPONT
==============

---------------------------------------
Programmeur backend, système et réseaux
---------------------------------------

**mail**: fd@shukuru.world ; **twitter**: @ShukuruWorld ; **git**: https://gitlab.com/shukuru

----

Programmeur autodidacte, passionné depuis de longues années, je suis en quête perpétuelle de savoir,
aussi bien dans le domaine des technologies numériques qu'en sciences physiques.

Connaître le fonctionnement des boîtes noires qui nous entourent au quotidien est pour
moi une nécessité ainsi qu'une véritable source de défis.
Ce dont pourquoi j'affectionne particulièrement le domaine de l'**électronique**, des **systèmes
d'exploitation** ainsi que des **communications** et aspire à devenir *kernel hacker* sur mon temps libre.

Résoudre des problèmes à un plus haut niveau d'abstraction, notamment pour le web orienté backend, 
est tout aussi intéressant lorsque cela permet **d'automatiser des tâches répétitives** 
ou tout simplement de **rendre la vie des gens plus agréable et fun** :)

Vie personnelle
^^^^^^^^^^^^^^^
- **Situation familiale**: 32 ans, pacsé et père de deux enfants.
- **Diplôme**: Brevet des Collèges (2004)
- **Permis**: B, C
- **Qualités**: Rigueur, Adaptabilité et autonomie
- **Défauts**: Attaché au détail, envahissant, impatient

Expériences professionnelles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**2009-2017**
    *Topographe pour application militaire* - **Ministère des Armées**

    .. admonition:: Compétences mises à l'épreuve
    
        Rigueur, adaptabilité, gestion humaine.

**2017-2019**
    *Magasinier matériel personnel* - **Ministère des Armées**
    
    .. admonition:: Compétences mises à l'épreuve

        Rigueur, adaptabilité, logique, gestion de matériel.
        J'y ai également mis mes **compétences en programmation à contribution** afin de faciliter le suivi des colis 
        pour l'ensemble de l'équipe.

**2019-2022**
    *Développeur backend, système et réseaux* - **Ministère des Armées**

    .. admonition:: Compétences mises à l'épreuve

        Rigueur, autonomie, architecture micro-services, API, Python, C, Flask, SQLAlchemy,
        Nagios, Nginx, ES6/Bootstrap/HTML5, Docker, GitLab CI, Documentation, MCO, MCS.

Compétences techniques
^^^^^^^^^^^^^^^^^^^^^^
**Langages de programmation**: C ; Python ; Assemblage x86

**Web**: Flask ; SQLAlchemy

**CI/CD**: GitLab CI (Pipelines, éxécuteurs auto-hébergés) ; Harbor Registry/Notary/Trivy

**Réseau**: HTTP(S) ; AMQP ; gRPC ; Sockets bas-niveau (TCP, UDP) ; Nginx ; WSGI

**Système**: Linux (Fedora, Debian) ; IPC (sockets UNIX) ; Docker ; Nagios ; Théorie des systèmes d'exploitation

**Méthodologies**: TDD ; Agile

Compétences en cours d'assimilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Système**: Kubernetes

Projets personnels
^^^^^^^^^^^^^^^^^^
- **GitLab**: https://gitlab.com/shukuru 
- **Notes personnelles:** https://unsignednotes.shukuru.world

Liste de quelques projets, terminés ou non:
    - Découpeur de fichiers (Python) https://gitlab.com/shukuru/slice
    - Générateur de flux RSS (Python) https://gitlab.com/shukuru/naranja-git
    - Unsigned Notes (Sphinx) https://gitlab.com/shukuru/unsigned-notes
    - Bootloader stage 1 pour architecture x86 (Assemblage x86) https://gitlab.com/shukuru/seed-bootloader
    - Désassembleur pour Intel 8080 https://gitlab.com/shukuru/e8080

Je passe beaucoup plus de temps à expérimenter qu'à produire des projets
personnels, mais je fais au mieux pour accommoder cela à ma vie de famille et
professionnelle :)

Langues
^^^^^^^
- **Anglais**: Lu, parlé (B2)
- **Espagnol**: Lu, parlé (B1)

Centres d’intérêts
^^^^^^^^^^^^^^^^^^
    - Sciences physiques et chimiques
    - Électronique
    - Voyages
    - Rédaction
    - Cinéma, Musique
